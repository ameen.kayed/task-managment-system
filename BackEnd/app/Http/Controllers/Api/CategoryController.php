<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\CategoryResource;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;

class CategoryController extends ApiController
{

    private $categoryRepository;
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index(Request $request)
    {
     
        $categoties = $this->categoryRepository
            ->getCategories($request);

        $categoties = $categoties->orderByDesc('id');

        return $this->respondSuccess(CategoryResource::collection($categoties->get()));
    }
}
